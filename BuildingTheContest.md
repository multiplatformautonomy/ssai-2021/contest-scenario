# Building the Contest

## Important Files:
 - missions/contest.xml
 - include/pytorch-demo/plugins/autonomy/CTFAutonomy/*
 - include/pytorch-demo/plugins/sensor/CTFSensor/*
 - src/plugins/autonomy/CTFAutonomy/*
 - src/plugins/sensor/CTFSensor/*

## Testing State Space:
Use `./contest.sh` to rebuild and re-run your contest.  The Sample Contestant Agent 1 and Sample Opponent Agent 1 are implemented to hover and print debug information.  All other sample agents simply hover.

If you don't want to rebuild for some reason, you can run `NO_REBUILD=1 ./contest.sh`