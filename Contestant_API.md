# Contestant/Opponent Autonomy Interface

## Setup

### 0. Clone or Download this Code Repository

For all future instructions, it will be assumed that your current directory is the root directory of this code repository.

### 1. Install Docker

Follow instructions here: https://docs.docker.com/get-docker/

### 2. Install Python 3

Follow the instructions here: https://www.python.org/downloads/

### 3. Build the Docker Image

```bash
docker build --tag multiplatformautonomy/contestant-api .
```

### 4. Activate the Python3 Virtual Environment

```bash
source ./ctf-venv/bin/activate
```

## Running the Simulation

```
docker run --rm --name contest -d -p 6379:6379 multiplatformautonomy/contestant-api
python3 ./contest.py
```

## Project Structure

### ./contestant/sample/agent*.py

The contestant agent files contain three functions that allow contestants to develop their own autonomy algorithms.

- `step()`
- `done()`
- `reset()`

### ./missions/contest.xml

This is the SCRIMMAGE mission that will be run.  Set up all of your rules, agents, and plugins here.  Make sure the number of agents matches those set up in your `./contest.py` file and contestant and opponent modules.

### ./contest.py

```plaintext
usage: contest.py [-h] [--contestant CONTESTANT] [--opponent OPPONENT]

optional arguments:
  -h, --help            show this help message and exit
  --contestant CONTESTANT, -c CONTESTANT
                        specify subdirectory of ./contestant that contains contestant code
  --opponent OPPONENT, -o OPPONENT
                        specify subdirectory of ./opponent that contains contestant code
```

This is the main script that gets run to communicate with the simulation server.  It sets up one CTFAgent class for each agent in the contest mission configuration file (./missions/contest.xml).  To create each agent, it looks to python files in subdirectories of the `./contestant` and `./opponent` folders.  These subdirectories should be set up as python modules with an `__init__.py` file that imports each agent type.  Each agent has its own step, reset, and done functions.
