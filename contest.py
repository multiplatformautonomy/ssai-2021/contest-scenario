#!/usr/bin/env python3
from contestclient import CTFClient
import argparse
import importlib

ap = argparse.ArgumentParser("contest.py")
ap.add_argument("--contestant", "-c", default="sample", help="specify subdirectory of ./contestant that contains contestant code")
ap.add_argument("--opponent", "-o", default="sample", help="specify subdirectory of ./opponent that contains contestant code")
ap.add_argument("--episodes", "-e", default=1, type=int, help="the number of episodes to run")
ap.add_argument("--test", "-t", action="store_true", default=False, help="run in test mode instead of training mode")

args = ap.parse_args()
contestant = importlib.import_module("contestant."+args.contestant)
opponent = importlib.import_module("opponent."+args.opponent)

agents = [
    contestant.Agent1(1),
    contestant.Agent2(1),
    contestant.Agent3(1),
    opponent.Agent1(2),
    opponent.Agent2(2),
    opponent.Agent3(2)
]

train_mode = not args.test

ctf_client = CTFClient(agents)
ctf_client.start(train_mode=train_mode, episodes=args.episodes)