#!/bin/bash
BUILD=0
REDIS_PORT=${REDIS_PORT:=6379}

PYTH='exit 1;'
PYTH_MSG='Error: Python 3 not in path as either `python` or `python3`.'

if [ "$(python -c 'import sys; print(sys.version_info[0])')" = "3" ]; then
    PYTH="python"
    PYTH_MSG='Python 3 found in path as `python`.'
elif [ "$(python3 -c 'import sys; print(sys.version_info[0])')" = "3" ]; then
    PYTH="python3"
    PYTH_MSG='Python 3 found in path as `python3`.'
fi

echo $PYTH_MSG
$PYTH --version

if [ -z "$NO_REBUILD" ]; then
    DOCKER_BUILDKIT=1 docker build -f ./dockerfiles/contest.Dockerfile --tag multiplatformautonomy/contest .
    BUILD=$?
fi

if [ "$BUILD" = "0" ]; then
    docker kill contest
    docker rm contest
    docker run --name contest -d -p 6901:6901 -p $REDIS_PORT:6379 multiplatformautonomy/contest
    docker logs -f contest &
    sleep 10
    echo -e "Starting Client Python Script.\nPlease be sure that python points to Python 3 and pip is available for that python version."
    $PYTH -m pip install --user -r requirements.txt
    $PYTH ./contest.py $@
fi
