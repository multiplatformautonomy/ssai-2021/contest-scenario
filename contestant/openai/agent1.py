import gym
from .contestopenai import CTFOpenAIGym, CTFOpenAIAgent

class Agent1(CTFOpenAIAgent):
    def run(self):
        # This gets run in the background to help you do your agent loop.
        # The first step in an OpenAI agent is to reset the gym environment.
        # Then, enter a loop and perform actions based on your RL or other
        # algorithms/heuristics.
        # Good luck!
        done = False
        obs = self.env.reset()
        reward = 0
        while not done:
            # Determine your actions:
            action = [0,0]
            # Perform the action:
            obs, reward, done, _ = self.env.step(action)

    def get_reward(self, observations):
        ###  Update Reward  ###
        # Do your reward shaping here based on the observation space.
        return 0

    # You may define other helper functions below:
    def helper_function(self, argument1, argument2, etc):
        return NotImplemented
