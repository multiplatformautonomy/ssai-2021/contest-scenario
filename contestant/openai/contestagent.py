# Example CTF Agent
class CTFAgent(object):
    def __init__(self, team):
        self.team = team

    def step(self, current_reward, score, observations):
        return NotImplemented
    
    def done(self, score_msg):
        return NotImplemented
    
    def reset(self, score, observations):
        return NotImplemented