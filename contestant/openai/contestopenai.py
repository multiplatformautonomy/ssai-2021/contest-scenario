from .contestagent import CTFAgent
import gym
from gym.spaces import Discrete, Tuple, Box
import numpy as np
import time
import threading

BLANK_ACTION = [0, 0] # Does nothing.

class CTFOpenAIGym(gym.Env):
    def __init__(self, agent):
        super(CTFOpenAIGym, self).__init__()
        # 9 Discrete actions 0=hover 1-8=move directionally
        self.action_space = Tuple([Discrete(9), Box(low=0,  high=agent.max_speed, shape=(1,))])

        # 89 features for a single agent, potentially get state of all 3 agents on your team
        self.observation_space = Box(low=-1000, high=1000, shape=(291,), dtype=np.uint8)

        self._ctf_agent = agent
        self._ctf_maximum_iterations = 1e10
        self.done = False
        self.iteration = 0
    
    def render(self):
        None # Ignore rendering

    def step(self, action):
        self._ctf_agent.action[self.iteration] = action # set action
        self.iteration += 1
        while self.iteration not in self._ctf_agent.observations:
            time.sleep(0.001) # wait for iteration to return
        if self._ctf_agent._done:
            self.done = True
        obs, rew, done = self._ctf_agent.observations, self._ctf_agent.reward, self.done
        return obs, rew, done, None

    def reset(self):
        while self._ctf_agent.iteration > 0:
            # We must wait for the simulation to end before resetting.
            self.step(BLANK_ACTION)
        self.iteration = 0
        self.done = False
        while self.iteration not in self._ctf_agent.observations:
            time.sleep(0.001) # wait to get reset observations
        return self._ctf_agent.observations[0]

class CTFOpenAIAgent(CTFAgent):
    def __init__(self, team):
        self.action = None
        self.observations = {}
        self.action = {}
        self.team = team
        self._done = False
        self.reward = {}
        self.iteration = 0
        self.thread = threading.Thread(target=self.run)
        if self.__class__.__name__ == 'Agent2':
            # sensor
            self.max_speed = 7
        else:
            # capturer and jammer. speeds are verified and normalized on server
            self.max_speed = 10
        self.env = CTFOpenAIGym(self)
        self.thread.start()

    def run(self):
        return NotImplemented

    def get_reward(self, observations):
        return NotImplemented
    
    def step(self, _current_reward, _score, observations):
        # _current reward and _score are not used in this contest
        self.observations[self.iteration] = observations
        self.reward[self.iteration] = self.get_reward(observations)
        while self.iteration not in self.action:
            time.sleep(0.001) # wait for action
        action = self.action[self.iteration]
        self.iteration += 1
        if self.iteration >= 50:
            # save memory, only keep last 50
            del self.action[self.iteration - 50]
            del self.observations[self.iteration - 50]
            del self.reward[self.iteration - 50]
        return action
    
    def done(self, score_msg):
        self._done = True

    def reset(self, score, observations):
        self.action = {}
        self.reward = {}
        self.observations = {self.iteration: observations}
        self.iteration = 0

