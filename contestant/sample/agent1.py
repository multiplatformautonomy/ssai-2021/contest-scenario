#!/usr/bin/env python3
import numpy as np
from .contestagent import CTFAgent
import time

class Agent1(CTFAgent):
    def step(self, current_reward, score, observations):
        action = [observations[73], 10]
        print("Contestant Step: action=%s\tobservations shape=%s\n\tObservations: %s" % (str(action), str(np.array(observations).shape), str(observations)))
        return action

    def done(self, score):
        print("Done: %i" % score)

    def reset(self, score, observations):
        print("Reset: %i" % score)