import redis
import json
import time
import threading
import os

def json_lambda_wrapper(fun):
    def wrapper(x):
        values = json.loads(x["data"])
        return fun(*values)
    return wrapper

# For shorthand
JLW = json_lambda_wrapper

class CTFClient(object):
    def __init__(self, ctf_agents, redis_password="default_redis_password"):
        self.ctf_agents = ctf_agents
        self.r = redis.Redis(host="localhost", port=os.environ.get("REDIS_PORT", "6379"), password=redis_password)
        self.p = self.r.pubsub()
        self.p.subscribe(**{"reset": JLW(self.reset)})
        self.p.subscribe(**{"observation": JLW(self.get_all_actions)})
        self.p.subscribe(**{"done": self.done})
        self.isDone = False
    
    def start(self, train_mode=True, episodes=1):
        def exception_handler(args):
            if args.exc_type == OSError:
                # we expect this one
                print("Finished simulation")
            else:
                raise args.exc_value
        threading.excepthook = exception_handler  # override native handler
        print("Starting...")
        self.thread = self.p.run_in_thread(sleep_time=0.001)
        self.r.set("max_episodes", episodes)
        self.r.set("train_mode", 1*train_mode)
        self.r.set("started", 1)
        while not self.isDone:
            time.sleep(3)
        self.stop()
    
    def stop(self):
        print("Stopping...")
        self.thread.stop()
        self.r.set("stopped", 1)
        self.p.close()
    
    def reset(self, score, observations):
        i = 0
        for agent in self.ctf_agents:
            agent.reset(score, observations[i])
            i+=1
        self.get_all_actions(0, score, observations)

    def get_all_actions(self, current_reward, score, observations):
        id = 0
        actions = []
        print(len(observations))
        for obx in observations:
            actions.append(self.ctf_agents[id].step(current_reward, score, obx))
            id += 1
        self.r.publish("step", json.dumps(actions))

    def done(self, score):
        for agent in self.ctf_agents:
           if type(score) is not int:
                agent.done(float(score["data"]))
           else:
                agent.done(score)
        self.isDone = True
