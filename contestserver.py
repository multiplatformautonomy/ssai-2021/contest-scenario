#!/usr/bin/env python3
import sys
import copy
import gym
import scrimmage.utils
from qlearn import QLearn
import time
import math
import os.path
import json
import redis
import numpy as np

env = None

def preprocess(observation):
    states = np.asarray([np.concatenate(np.flipud(x)).tolist() for x in observation])
    allstates = []
    for i in range(len(states)):
        agentx = states[i]
        t = agentx[-1]
        tstates = [states[j] for j in range(len(states)) if states[j][-1] == t and j != i]
        n_teammates = len(tstates)
        mask = agentx[-(n_teammates+1):-1][::-1] # ..., n_teammates LOS flags, team_id, END.
        """
           This next line selects either a zeros vector of the appropriate length
           or the state vector depending on the mask value.
        """
        masked_tstates = [
            [
                [0]*len(tstates[j]), # zeros vector
                tstates[j]           # state vector
            ][int(mask[j])]               # select based on mask
            for j in range(n_teammates) # list comprehension
        ]

        # Append this state to the list of all states.
        allstates.append(np.concatenate([agentx, *masked_tstates]).tolist())
    return allstates


def test_openai():
    global env
    try:
        env = gym.make('scrimmage-ctf-v0')
    except gym.error.Error:
        mission_file = scrimmage.utils.find_mission('contest.xml')

        gym.envs.register(
            id='scrimmage-ctf-v1',
            entry_point='scrimmage.bindings:ScrimmageOpenAIEnv',
            max_episode_steps=1e9,
            reward_threshold=1e9,
            kwargs={"mission_file": mission_file}
        )
        env = gym.make('scrimmage-ctf-v1')
    redis_obj = redis.Redis(password=os.environ["REDIS_PASSWORD"])
    pubsub = redis_obj.pubsub()
    started = redis_obj.get("started")
    while not started:
        started = redis_obj.get("started")
    observation = env.reset()
    redis_obj.set("score", 0)
    pubsub.subscribe("step")
    stopped = redis_obj.get("stopped")
    redis_obj.set("episode", 1)
    print("reset")
    redis_obj.publish("reset", json.dumps([0, preprocess(observation)]))
    for message in pubsub.listen():
        if stopped:
            break
        elif type(message["data"]) == int:
            print("Integer message: %i" % message["data"])
        else:
            user_action_callback(message)
    pubsub.close()
    env.close()

def user_action_callback(message):
    global env
    redis_obj = redis.Redis(password=os.environ["REDIS_PASSWORD"])
    train_mode = (int(redis_obj.get('train_mode')) == 1)
    print(message)
    print(message["data"])
    actions = json.loads(message["data"])
    print(actions)
    obs, current_reward, done, _ = env.step(actions)
    episode = int(redis_obj.get("episode"))
    max_episodes = int(redis_obj.get("max_episodes"))
    score = float(redis_obj.get("score")) + current_reward
    redis_obj.set("score", score)
    if done:
        obs = env.reset()
        if episode < max_episodes:
            redis_obj.set("score", 0.0)
            redis_obj.incr("episode")
            redis_obj.publish("reset", json.dumps([score, preprocess(obs)]))
        else:
            redis_obj.publish("done", score)
    else:
        redis_obj.publish("observation", json.dumps([current_reward, score, preprocess(obs)]))
    redis_obj.close()

if __name__ == '__main__':
    print("=== CTF Server ===")
    start_time = time.time()
    test_openai()
    print("total time: ", time.time() - start_time)
