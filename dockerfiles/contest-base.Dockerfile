FROM multiplatformautonomy/vscrimmage-vnc:v0.11.0

LABEL MAINTAINER="Frazier N Baker <frazier.baker@gtri.gatech.edu>"

ENV REDIS_PASSWORD "default_redis_password"

USER 0
RUN curl -o redis-stable.tar.gz http://download.redis.io/redis-stable.tar.gz && \
    tar -xzf redis-stable.tar.gz && \
    cd redis-stable && \
    make && \
    make install
    
RUN source ~/.scrimmage/setup.bash && python3 -m pip install redis gym