/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#ifndef INCLUDE_MY_SCRIMMAGE_PLUGINS_PLUGINS_AUTONOMY_CREATEBUILDING_CREATEBUILDING_H_
#define INCLUDE_MY_SCRIMMAGE_PLUGINS_PLUGINS_AUTONOMY_CREATEBUILDING_CREATEBUILDING_H_
#include <scrimmage/autonomy/Autonomy.h>
#include <scrimmage/plugins/interaction/Boundary/BoundaryBase.h>
#include <scrimmage/proto/Shape.pb.h>

#include <string>
#include <map>
#include <utility>
#include <memory>

namespace scrimmage {
namespace autonomy {
class CreateBuilding : public scrimmage::Autonomy {
 public:
  CreateBuilding();
  void init(std::map<std::string, std::string> &params) override;
  bool step_autonomy(double t, double dt) override;

 protected:
  void draw_cuboid(double t, double dt);

  std::map<int, std::pair<scrimmage_proto::Shape, std::shared_ptr<interaction::BoundaryBase>>> boundaries_;
  int boundary_id_;
  scrimmage_proto::ShapePtr cuboid_shape_;
  double x_center_, y_center_, z_center_;
  double x_length_, y_length_, z_length_;
  scrimmage::PublisherPtr building_size_pub_;

};
} // namespace autonomy
} // namespace scrimmage
#endif // INCLUDE_MY_SCRIMMAGE_PLUGINS_PLUGINS_AUTONOMY_CREATEBUILDING_CREATEBUILDING_H_
