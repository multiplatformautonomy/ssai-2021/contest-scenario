/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#ifndef INCLUDE_PYTORCH_DEMO_PLUGINS_SENSOR_CTFSENSOR_CTFSENSOR_H_
#define INCLUDE_PYTORCH_DEMO_PLUGINS_SENSOR_CTFSENSOR_CTFSENSOR_H_

#include <scrimmage/sensor/Sensor.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/entity/Contact.h>
#include <scrimmage/plugins/sensor/ScrimmageOpenAISensor/ScrimmageOpenAISensor.h>
#include <scrimmage/plugins/interaction/Boundary/BoundaryBase.h>
#include <vector>
#include <map>
#include <string>

namespace scrimmage {
   namespace interaction {
      class BoundaryBase;
   }
   namespace sensor {
      class CTFSensor : public scrimmage::sensor::ScrimmageOpenAISensor {
      public:
         void init(std::map<std::string, std::string> &params);
         void set_observation_space() override;
         void get_observation(int* data, uint32_t beg_idx, uint32_t end_idx) override;
         void get_observation(double* data, uint32_t beg_idx, uint32_t end_idx) override;
      protected:
         void find_closest_enemy();

         // Checks if `agent` is in range of the sensor
         bool is_in_range(Contact& agent);

         // Determines the heading bin that the current agent must take to get to `obj`
         int get_heading_bin_to(const Eigen::Vector3d& obj);
         int get_heading_opposite_side();
         
         // Returns whether parent entity has unobstructed line of sight to the contact
         bool check_los(sc::Contact& contact);
         
         // Returns whether or not in base of given id.
         bool in_base(const int& boundary_id);

         // Gets team space
         int get_team_space();

         // Converts `vec` into an integer heading
         int get_heading_bin(const Eigen::Vector3d& vec);
         const int HEADING_SECTORS = 8;

         std::map<int, std::pair<scrimmage_proto::Shape, std::shared_ptr<interaction::BoundaryBase>>> boundaries_;
         std::map<int, std::pair<double, double>> building_sizes_;
         int building_team_ = 3;
         uint building_count_ = 0;
         int team_space_boundary_id_ = 1;
         int enemy_space_boundary_id_ = 2;
         double sensing_range_ = 10;
         Eigen::Vector3d my_flag_location_;
         Eigen::Vector3d enemy_flag_location_;
         int flag_id_ = 0;
         int enemy_flag_id_ = 0;
         std::set<int> seen_ent_ids_; //ids of buildings or enemy flag that have been seen 
         int y_extent_ = 200;
         int x_extent_ = 500;
         int has_flag_ = 0, jammed_ = 0, team_space_ = 0;
         std::vector<scrimmage::ID> jammed_ids_;
         sc::StatePtr closest_ = nullptr;

         scrimmage::PublisherPtr pub_flag_dist_;
      }; 
   } // namespace sensor
} // namespace scrimmage
#endif // INCLUDE_PYTORCH_DEMO_PLUGINS_SENSOR_CTFSENSOR_CTFSENSOR_H_
