The ContestMetrics plugin subscribes to the following publishers to compute the following information:

FlagTaken: counts the number of flags taken
FlagCaptured: counts the number of flags captured
Jamming: total time jammed
FlagDist: current distance to the flag

Notes:
- The components of the score are recorded on a per-entity basis and then all the entities on a given team are combined at the end of a match. For the first three, we add up the numbers while for flag distance, we take the minimum of all the numbers.
- The jamming score is the total amount of time each entity in a team has been jammed by the other team. Therefore it should count negatively.
- Each time a message from FlagDist is received, it *replaces* the old distance. The rationale is that we want the distance at the end of a match. The reason we still use a subscriber is that ContestMetrics does not have access to the entities to get the distances at the end.
- I added the FlagDist publisher to the CTFSensor plugin. When the class receives the flag location, it also publishes its distance to the flag.
