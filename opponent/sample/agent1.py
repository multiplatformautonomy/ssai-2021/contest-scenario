#!/usr/bin/env python3
import numpy as np
from .contestagent import CTFAgent

class Agent1(CTFAgent):
    def step(self, current_reward, score, observations):
        action = [0, 0]
        print("Opponent Otherside: %d" % observations[73])
        return action

    def done(self, score):
        print("Done: %i" % score)

    def reset(self, score, observations):
        print("Reset: %i" % score)