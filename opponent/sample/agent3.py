#!/usr/bin/env python3
from .contestagent import CTFAgent

class Agent3(CTFAgent):
    def step(self, current_reward, score, observations):
        return [1, 0]

    def done(self, score):
        print("Done: %i" % score)

    def reset(self, score, observations):
        print("Reset: %i" % score)