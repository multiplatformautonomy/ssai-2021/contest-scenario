# Example CTF Agent
class CTFAgent(object):
    def __init__(self, team):
        self.team = team

    def step(self, current_reward, score, observations):
        NotImplemented
    
    def done(self, score_msg):
        NotImplemented
    
    def reset(self, score, observations):
        NotImplemented