/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/autonomy/CTFAutonomy/CTFAutonomy.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/pubsub/Publisher.h>
#include <scrimmage/plugins/interaction/Boundary/Boundary.h>
#include <scrimmage/common/Waypoint.h>
#include <scrimmage/plugins/autonomy/WaypointGenerator/WaypointList.h>
#include <scrimmage/msgs/Capture.pb.h>
#include <scrimmage/parse/MissionParse.h>

#include <cmath>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;
namespace sci = scrimmage::interaction;

REGISTER_PLUGIN(scrimmage::Autonomy,
                scrimmage::autonomy::CTFAutonomy,
                CTFAutonomy_plugin)

namespace scrimmage {
namespace autonomy {

  void CTFAutonomy::init_helper(std::map<std::string, std::string> &params) {

    output_vel_x_idx_ = vars_.declare(VariableIO::Type::velocity_x, VariableIO::Direction::Out);
    output_vel_y_idx_ = vars_.declare(VariableIO::Type::velocity_y, VariableIO::Direction::Out);
    output_vel_z_idx_ = vars_.declare(VariableIO::Type::velocity_z, VariableIO::Direction::Out);

    dt = parent()->mp()->dt();
    game_boundary_id_ = get<int>("game_boundary_id",params,game_boundary_id_);
    team_space_boundary_id_ = sc::get<int>("team_space_boundary_id", params, team_space_boundary_id_);
    max_speed_ = get<int>("max_speed", params, max_speed_);

    // Set all the options to unit vectors in the desired direction
    const double mag = std::sqrt(2);

    action_options_[0] = Eigen::Vector2d( 1, 0);		// Heading 0
    action_options_[1] = Eigen::Vector2d( 1, 1) / mag;	// Heading 1
    action_options_[2] = Eigen::Vector2d( 0, 1);		// Heading 2
    action_options_[3] = Eigen::Vector2d(-1, 1) / mag;	// Heading 3
    action_options_[4] = Eigen::Vector2d(-1, 0);		// Heading 4
    action_options_[5] = Eigen::Vector2d(-1,-1) / mag;	// Heading 5
    action_options_[6] = Eigen::Vector2d( 0,-1);		// Heading 6
    action_options_[7] = Eigen::Vector2d( 1,-1) / mag;	// Heading 7

    auto callback = [&] (scrimmage::MessagePtr<sp::Shape> msg) {
        std::shared_ptr<sci::BoundaryBase> boundary = sci::Boundary::make_boundary(msg->data);
        boundaries_[msg->data.id().id()] = std::make_pair(msg->data, boundary);
    };
    subscribe<sp::Shape>("GlobalNetwork", "Boundary", callback);

    auto flag_taken_cb = [&] (scrimmage::MessagePtr<std::tuple<int,int,int>> msg) {
        if (std::get<0>(msg->data) == parent_->id().id()) {
            has_flag_ = true;
        }
    };
    subscribe<std::tuple<int,int,int>>("GlobalNetwork", "FlagTaken", flag_taken_cb);

    auto flag_captured_cb = [&] (scrimmage::MessagePtr<int> msg) {
        if (msg->data == parent_->id().team_id()) {
            flag_captured_ = true;
        }
        else{
            enemy_won_ = true;
        }
    };
    subscribe<int>("GlobalNetwork", "FlagCaptured", flag_captured_cb);

    auto building_cb = [&] (scrimmage::MessagePtr<std::tuple<int,double,double>> msg ) {
        std::tuple<int,double,double> info = msg->data;
        int id = std::get<0>(info);
        double x_length = std::get<1>(info);
        double y_length = std::get<2>(info);
		auto contact_it = (*contacts_).find(id);
		if (contact_it != (*contacts_).end())
		{
			Eigen::Vector3d pos = contact_it->second.state()->pos();
			building_map_[id] = std::tuple<double,double,double,double>(pos[0],pos[1],x_length,y_length);
		}
    };
    subscribe<std::tuple<int,double,double>>("GlobalNetwork","BuildingSize",building_cb);

    flag_captured_pub_ = advertise("GlobalNetwork", "FlagCaptured");
    return_dist_pub_ = advertise("GlobalNetwork", "ReturnDistance");
  }

  void CTFAutonomy::set_environment() {
      // Note: a default reward range set to [-inf,+inf] already exists. Set it if you want a narrower range.
      // reward_range = std::make_pair(0, 2);
    action_space.discrete_count.push_back(DIRECTION_COUNT);
    action_space.continuous_extrema.emplace_back(0, max_speed_);
  }

  std::tuple<bool, double, pybind11::dict> CTFAutonomy::calc_reward() {
    // pybind11::list obs = observations_.observation.cast<pybind11::list>();
      double reward = 0;
      pybind11::dict info;
      // Done: return std::make_tuple(true, reward, info);
      auto it = boundaries_.find(team_space_boundary_id_);
      bool in_base = it != boundaries_.end() && std::get<1>(it->second)->contains(parent_->state()->pos()); 

      // returning distance
      if(has_flag_ && it != boundaries_.end()) {
        // first get the x coord of the team space boundary
        auto extents = std::get<1>(it->second)->extents();
        auto extents_x = extents.at(0); // x coordinates of the ends of the team space
        // pick the coordinate with smaller absolute value
        auto edge_x = abs(std::get<0>(extents_x)) < abs(std::get<1>(extents_x)) ? std::get<0>(extents_x) : std::get<1>(extents_x);
        
        auto msg = std::make_shared<sc::Message<std::tuple<int, int, double>>>();
        double dist = edge_x - parent()->state()->pos().x();
        msg->data = std::tuple<int,int,double>(parent()->id().id(), parent()->id().team_id(), dist);
        return_dist_pub_->publish(msg);
      }

      if(has_flag_ && in_base) {
        auto msg = std::make_shared < sc::Message<int> >();
        msg->data = parent_->id().team_id();
        flag_captured_pub_->publish(msg);
      }
      return std::make_tuple(flag_captured_, reward, info);
  }

  bool CTFAutonomy::step_helper() {
      const Eigen::Vector2d result = action_options_[action.discrete[0]] * action.continuous[0];
      Eigen::Vector3d desired_vel(result(0),result(1),0);
      Eigen::Vector3d proj_vel = desired_vel.normalized()*max_speed_;
      Eigen::Vector3d proj_pos = parent()->state()->pos()+proj_vel*dt;
      auto it = boundaries_.find(game_boundary_id_);
      bool in_game = it != boundaries_.end() && std::get<1>(it->second)->contains(proj_pos);
      bool building_collision = false;
      for (auto it = building_map_.begin(); it!=building_map_.end(); it++)
      {
          double x_pos,y_pos,x_length,y_length;
          x_pos = std::get<0>(it->second);
          y_pos = std::get<1>(it->second);
          x_length = std::get<2>(it->second);
          y_length = std::get<3>(it->second);
          building_collision = ((proj_pos[0] > (x_pos-x_length/2)) & (proj_pos[0] < (x_pos+x_length/2))) && ((proj_pos[1] > (y_pos-y_length/2)) & (proj_pos[1] < (y_pos+y_length/2)));
          if (building_collision) 
		  {
			  break;
		  }
      }
      in_game = in_game && !building_collision;
      if (in_game)
      {
          vars_.output(output_vel_x_idx_, result(0));
          vars_.output(output_vel_y_idx_,result(1));
      }
      else
      {
          vars_.output(output_vel_x_idx_,0);
          vars_.output(output_vel_y_idx_,0);
      }
	  return true;
  }

} // namespace autonomy
} // namespace scrimmage
