/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/autonomy/CreateBuilding/CreateBuilding.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>
#include <scrimmage/math/Quaternion.h>
#include <scrimmage/proto/Shape.pb.h>
#include <scrimmage/proto/ProtoConversions.h>
#include <scrimmage/math/Angles.h>
#include <scrimmage/plugins/interaction/Boundary/Boundary.h>
#include <scrimmage/pubsub/Publisher.h>
#include <scrimmage/pubsub/Message.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/common/Waypoint.h>
#include <scrimmage/msgs/Capture.pb.h>

#include <iostream>
#include <limits>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;
namespace sci = scrimmage::interaction;

REGISTER_PLUGIN(scrimmage::Autonomy,
                scrimmage::autonomy::CreateBuilding,
                CreateBuilding_plugin)

namespace scrimmage {
namespace autonomy {

CreateBuilding::CreateBuilding() {}

void CreateBuilding::init(std::map<std::string, std::string> &params) {
  x_length_ = sc::get<double>("x_length", params, x_length_);
  y_length_ = sc::get<double>("y_length", params, y_length_);
  z_length_ = sc::get<double>("z_length", params, z_length_);

  boundary_id_ = sc::get<int>("boundary_id", params, boundary_id_);

  auto callback = [&](scrimmage::MessagePtr<sp::Shape> msg) {
//    std::shared_ptr<sci::BoundaryBase> boundary = sci::Boundary::make_boundary(msg->data);
//    boundaries_[msg->data.id().id()] = std::make_pair(msg->data, boundary);

    if (msg->data.id().id() == boundary_id_) {
      // Randomly choose a location inside the boundary
      double min_x = msg->data.cuboid().center().x() - msg->data.cuboid().x_length() / 2 + x_length_;
      double min_y = msg->data.cuboid().center().y() - msg->data.cuboid().y_length() / 2 + y_length_;
      double max_x = msg->data.cuboid().center().x() + msg->data.cuboid().x_length() / 2 - x_length_;
      double max_y = msg->data.cuboid().center().y() + msg->data.cuboid().y_length() / 2 - y_length_;

      double rand_x = (double) rand() / RAND_MAX, rand_y = (double) rand() / RAND_MAX;

      x_center_ = min_x + rand_x * (max_x - min_x);
      y_center_ = min_y + rand_y * (max_y - min_y);

      state_->pos() = Eigen::Vector3d(x_center_, y_center_, state_->pos()(2));
    }
  };
  subscribe<sp::Shape>("GlobalNetwork", "Boundary", callback);

  desired_state_->quat().set(0, 0, state_->quat().yaw());
  desired_state_->pos() = Eigen::Vector3d::UnitZ() * state_->pos()(2);

  cuboid_shape_ = std::make_shared<scrimmage_proto::Shape>();

  building_size_pub_ = advertise("GlobalNetwork", "BuildingSize");
}
bool CreateBuilding::step_autonomy(double t, double dt) {
  draw_cuboid(t, dt);
  std::tuple<int, double, double> info(parent_->id().id(), x_length_, y_length_);
  auto msg = std::make_shared<sc::Message<std::tuple<int, double, double>>>(info);
  msg->data = info;
  building_size_pub_->publish(msg);
  return true;
}
// Find nearest entity on other team. Loop through each contact, calculate
// distance to entity, save the ID of the entity that is closest.
//    double min_dist = std::numeric_limits<double>::infinity();
//    for (auto it = contacts_->begin(); it != contacts_->end(); it++) {

// Skip if this contact is on the same team

void CreateBuilding::draw_cuboid(double t, double dt) {
    cuboid_shape_->set_opacity(1.0);
	sc::set(cuboid_shape_->mutable_cuboid()->mutable_center(), state_->pos()(0), state_->pos()(1), state_->pos()(2));
	cuboid_shape_->mutable_cuboid()->set_x_length(x_length_);
	cuboid_shape_->mutable_cuboid()->set_y_length(y_length_);
	cuboid_shape_->mutable_cuboid()->set_z_length(z_length_);

  sc::Quaternion quat(sc::Angles::deg2rad(0.0),
                      sc::Angles::deg2rad(0.0),
                      sc::Angles::deg2rad(0.0));
  sc::set(cuboid_shape_->mutable_cuboid()->mutable_quat(), quat);

  cuboid_shape_->set_persistent(true);
  sc::set(cuboid_shape_->mutable_color(), 0, 150, 150);
  draw_shape(cuboid_shape_);

}

} // namespace autonomy
} // namespace scrimmage
