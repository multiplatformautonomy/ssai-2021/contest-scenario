/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/interaction/CTFInteraction/CTFInteraction.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/common/Utilities.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>

#include <scrimmage/pubsub/Message.h>
#include <scrimmage/pubsub/Publisher.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/msgs/Capture.pb.h>

#include <scrimmage/plugins/interaction/Boundary/Boundary.h>

#include <memory>
#include <limits>
#include <set>
#include <iostream>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;

REGISTER_PLUGIN(scrimmage::EntityInteraction,
                scrimmage::interaction::CTFInteraction,
                CTFInteraction_plugin)

namespace scrimmage {
namespace interaction {
    CTFInteraction::CTFInteraction() {
}

bool CTFInteraction::init(std::map<std::string, std::string> &mission_params,
                                  std::map<std::string, std::string> &plugin_params) {
    flag_taken_pub_ = advertise("GlobalNetwork", "FlagTaken");

    capture_range_ = sc::get<double>("capture_range", plugin_params, capture_range_);
    
    if(!sc::get_vec<int>("capturer_ids", plugin_params, ",", capturer_ids_)) {
        std::cout << "No capturers passed!" << endl;
        return false;
    }
    for (int cap_id : capturer_ids_) {
        capturers_.insert(cap_id);
    }
    
    auto flag_cb = [&] (scrimmage::MessagePtr<std::tuple<int, int, Eigen::Vector3d>> msg) {
        int team_id = std::get<0>(msg->data);
        int flag_id = std::get<1>(msg->data);
        Eigen::Vector3d location = std::get<2>(msg->data);
        flag_locations_[team_id] = location;
        std::cout << "Team " << team_id << " Flag Location: " << location << std::endl;
    };
    subscribe<std::tuple<int, int, Eigen::Vector3d>>("GlobalNetwork", "FlagLocation", flag_cb);
    return true;
}

bool CTFInteraction::step_entity_interaction(std::list<sc::EntityPtr> &ents,
                                                     double t, double dt) {
    for (sc::EntityPtr ent : ents) {
        int entity_id = ent->id().id();
        int ent_team_id = ent->id().team_id();
        if (capturers_.find(entity_id) != capturers_.end()) { // check if capturer agent
            for (auto &kv : flag_locations_) {
                int flag_team = kv.first;
                Eigen::Vector3d loc = kv.second;
                if (flag_team != ent_team_id) { // only take enemy flags
                    double dist = (loc - ent->state()->pos()).norm();
                    if (dist <= capture_range_ && !has_flag_[entity_id]) {
                        has_flag_[entity_id] = true;
                        auto msg = std::make_shared<sc::Message<std::tuple<int,int,int>>>();
                        msg->data = std::tuple<int,int,int>(entity_id, ent_team_id, flag_team);
                        flag_taken_pub_->publish(msg); // Flag Taken!!
                    }
                }
            }
        }
    }
    return true;
}

} // namespace interaction
} // namespace scrimmage
