/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/interaction/JammingInteraction/JammingInteraction.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/common/Utilities.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>

#include <scrimmage/pubsub/Message.h>
#include <scrimmage/pubsub/Publisher.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/msgs/Capture.pb.h>

#include <scrimmage/plugins/interaction/Boundary/Boundary.h>

#include <memory>
#include <limits>
#include <iostream>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;

REGISTER_PLUGIN(scrimmage::EntityInteraction,
    scrimmage::interaction::JammingInteraction,
    JammingInteraction_plugin
)

namespace scrimmage {
namespace interaction {

JammingInteraction::JammingInteraction() {
}

bool JammingInteraction::init(std::map<std::string, std::string> &mission_params,
                              std::map<std::string, std::string> &plugin_params) {
  jamming_range_ = sc::get<double>("jamming_range", plugin_params, jamming_range_);
  if(!sc::get_vec<int>("jammer_ids", plugin_params, ",", jammer_ids_)) {
    cout << "No jammers passed!" << endl;
    return false;
  }
  if(!sc::get_vec<int>("flag_ids", plugin_params, ",", flag_ids_)) {
    cout << "No flags passed!" << endl;
    return false;
  }

  pub_jamming_ = advertise("GlobalNetwork", "Jamming");

  return true;
}

bool JammingInteraction::step_entity_interaction(std::list<sc::EntityPtr> &ents,
                                                 double t, double dt) {
  //If an enemy platform is within jamming_range of jammer, the platform's ID will be added to a vector
  // that will be published and received by a subscriber in CTFSensor.
  std::vector<scrimmage::ID> jammed_agents; // a vector to contain the id's of all the jammed agents
  std::vector<sc::EntityPtr> jammers;

  for (auto ent : ents) {
    if (std::find(jammer_ids_.begin(), jammer_ids_.end(), ent->id().id()) != jammer_ids_.end()) {
      jammers.push_back(ent);
    }
  }

  for (auto jammer : jammers) {
    for (sc::EntityPtr ent : ents) {
      if (ent->id().team_id() == jammer->id().team_id()) {
        continue; // Ignore same team
      }
      if (std::find(flag_ids_.begin(), flag_ids_.end(), ent->id().id()) != flag_ids_.end()) {
        continue; // Ignore flags
      }
      if ((jammer->state()->pos() - ent->state()->pos()).norm() <= jamming_range_) {
        jammed_agents.push_back(ent->id());
      }
    }
  }
  auto msg = std::make_shared < sc::Message < std::vector < scrimmage::ID>>>(jammed_agents);
  msg->data = jammed_agents;
  pub_jamming_->publish(msg);

  return true;
}
} // namespace interaction
} // namespace scrimmage
