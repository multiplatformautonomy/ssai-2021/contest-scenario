/*!
 * @file
 *
 * @section LICENSE
 *
 * Copyright (C) 2017 by the Georgia Tech Research Institute (GTRI)
 *
 * This file is part of SCRIMMAGE.
 *
 *   SCRIMMAGE is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU Lesser General Public License as published by the
 *   Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *   SCRIMMAGE is distributed in the hope that it will be useful, but WITHOUT
 *   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *   License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SCRIMMAGE.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin DeMarco <kevin.demarco@gtri.gatech.edu>
 * @author Eric Squires <eric.squires@gtri.gatech.edu>
 * @date 31 July 2017
 * @version 0.1.0
 * @brief Brief file description.
 * @section DESCRIPTION
 * A Long description goes here.
 *
 */

#include <pytorch-demo/plugins/metrics/ContestMetrics/ContestMetrics.h>

#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/entity/Entity.h>
#include <scrimmage/math/State.h>
#include <scrimmage/parse/ParseUtils.h>
#include <scrimmage/common/Utilities.h>
#include <scrimmage/metrics/Metrics.h>
#include <scrimmage/msgs/Capture.pb.h>

#include <scrimmage/pubsub/Message.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/msgs/Collision.pb.h>
#include <scrimmage/msgs/Event.pb.h>

#include <iostream>
#include <limits>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;

REGISTER_PLUGIN(scrimmage::Metrics,
                scrimmage::metrics::ContestMetrics,
                ContestMetrics_plugin)

namespace scrimmage {
namespace metrics {

ContestMetrics::ContestMetrics() {
}

void ContestMetrics::init(std::map<std::string, std::string> &params) {
    params_ = params;
    t_ = 0;
    dt_ = 0;

    auto flag_taken_cb = [&] (scrimmage::MessagePtr<std::tuple<int,int,int>> msg) {
        scores_[std::get<0>(msg->data)].increment_flags_taken();
    };
    subscribe<std::tuple<int,int,int>>("GlobalNetwork", "FlagTaken", flag_taken_cb);

    auto flag_captured_cb = [&] (scrimmage::MessagePtr<int> msg) {
        int team_id = msg->data;
        // Create the score, if necessary
        if (team_coll_scores_.count(team_id) == 0) {
            Score score;
            score.set_weights(params_);
            team_coll_scores_[team_id] = score;
        }
        team_coll_scores_[team_id].increment_flags_captured();
    };
    subscribe<int>("GlobalNetwork", "FlagCaptured", flag_captured_cb);

    auto jamming_cb = [&] (scrimmage::MessagePtr<std::vector<scrimmage::ID>> msg) {
        // msg contains a list of jammed ids
        for (size_t i = 0; i < msg->data.size(); i++) {
            scores_[msg->data[i].id()].add_time_jammed(dt_);
        }
        dt_ = 0;
    };
    subscribe<std::vector<scrimmage::ID>>("GlobalNetwork", "Jamming", jamming_cb);

    auto return_dist_cb = [&] (scrimmage::MessagePtr<std::tuple<int, int, double>> msg) {
        // msg contains a tuple of (entity id, team id, distance)
        int id = std::get<0>(msg->data);
        double dist = std::get<2>(msg->data);
        scores_[id].set_return_distance(dist);
    };
    subscribe<std::tuple<int, int, double>>("GlobalNetwork", "ReturnDistance", return_dist_cb);

    auto flag_dist_cb = [&] (scrimmage::MessagePtr<std::tuple<int, int, double>> msg) {
        // msg contains a tuple of (team id, entity id, distance)
        int id = std::get<1>(msg->data);
        double dist = std::get<2>(msg->data);
        scores_[id].set_flag_closeness(dist);
    };
    subscribe<std::tuple<int, int, double>>("GlobalNetwork", "FlagDist", flag_dist_cb);
}

bool ContestMetrics::step_metrics(double t, double dt) {
    t_ = t;
    dt_ = dt_ + dt;
    return true;
}

void ContestMetrics::calc_team_scores() {
    for (auto &kv : scores_) {
        Score &score = kv.second;
        int team_id = (*id_to_team_map_)[kv.first];

        // Create the score, if necessary
        if (team_coll_scores_.count(team_id) == 0) {
            Score score;
            score.set_weights(params_);
            team_coll_scores_[team_id] = score;
        }
        team_coll_scores_[team_id].add_flags_taken(score.flags_taken());
        team_coll_scores_[team_id].add_time_jammed(score.time_jammed());
        team_coll_scores_[team_id].update_return_distance(score.return_distance());
        team_coll_scores_[team_id].update_flag_closeness(score.flag_closeness());
    }

    for (auto &kv : team_coll_scores_) {
        int team_id = kv.first;
        Score &score = kv.second;
        team_metrics_[team_id]["flags_taken"] = score.flags_taken();
        team_metrics_[team_id]["flags_captured"] = score.flags_captured();
        team_metrics_[team_id]["time_jammed"] = score.time_jammed();
        team_metrics_[team_id]["return_dist"] = score.return_distance();
        team_metrics_[team_id]["min_flag_dist"] = score.flag_closeness();
        team_scores_[team_id] = score.score();
    }

    // list the headers we want put in the csv file
    headers_.push_back("flags_taken");
    headers_.push_back("flags_captured");
    headers_.push_back("time_jammed");
    headers_.push_back("return_distance");
    headers_.push_back("min_flag_dist");
}

void ContestMetrics::print_team_summaries() {
    for (std::map<int, Score>::iterator it = team_coll_scores_.begin();
         it != team_coll_scores_.end(); ++it) {

        cout << "Score: " << it->second.score() << endl;
        cout << "Flags Taken: " << it->second.flags_taken() << endl;
        cout << "Flags Captured: " << it->second.flags_captured() << endl;
        cout << "Time jammed: " << it->second.time_jammed() << endl;
        cout << "Return Distance: " << it->second.return_distance() << endl;
        cout << "Minimum Flag Distance: " << it->second.flag_closeness() << endl;
        cout << sc::generate_chars("-", 70) << endl;
    }
}
} // namespace metrics
} // namespace scrimmage
