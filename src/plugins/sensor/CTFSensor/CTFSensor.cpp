#include <pytorch-demo/plugins/sensor/CTFSensor/CTFSensor.h>

#include <scrimmage/math/State.h>
#include <scrimmage/common/Time.h>
#include <scrimmage/plugin_manager/RegisterPlugin.h>
#include <scrimmage/plugins/interaction/Boundary/Boundary.h>
#include <scrimmage/pubsub/Subscriber.h>
#include <scrimmage/pubsub/Publisher.h>
#include <scrimmage/pubsub/Message.h>
#include <scrimmage/common/Shape.h>

#include <map>
#include <string>
#include <vector>
#include <list>
#include <scrimmage/msgs/Capture.pb.h>
#include <scrimmage/parse/ParseUtils.h>

using std::cout;
using std::endl;

namespace sc = scrimmage;
namespace sm = scrimmage_msgs;
namespace sci = scrimmage::interaction;

REGISTER_PLUGIN(scrimmage::Sensor,
                scrimmage::sensor::CTFSensor,
                CTFSensor_plugin)

namespace scrimmage {
namespace sensor {
    void CTFSensor::init(std::map<std::string, std::string> &params) {
        sensing_range_ = get<double>("sensing_range", params, sensing_range_);
        building_count_ = sc::get<int>("building_count", params, building_count_);
        team_space_boundary_id_ = sc::get<int>("team_space_boundary_id", params, team_space_boundary_id_);
        enemy_space_boundary_id_ = sc::get<int>("enemy_space_boundary_id", params, enemy_space_boundary_id_);
        flag_id_ = sc::get<int>("flag_id", params, flag_id_);
        pub_flag_dist_ = advertise("GlobalNetwork", "FlagDist");

        auto flag_taken_cb = [&] (scrimmage::MessagePtr<std::tuple<int,int,int>> msg) {
            if (std::get<0>(msg->data) == parent_->id().id()) {
                has_flag_ = 1;
            }
        };
        subscribe<std::tuple<int,int,int>>("GlobalNetwork", "FlagTaken", flag_taken_cb);

        auto boundary_cb = [&] (scrimmage::MessagePtr<sp::Shape> msg) {
            std::shared_ptr<sci::BoundaryBase> boundary = sci::Boundary::make_boundary(msg->data);
            boundaries_[msg->data.id().id()] = std::make_pair(msg->data, boundary);
        };
        subscribe<sp::Shape>("GlobalNetwork", "Boundary", boundary_cb);

        auto jamming_cb = [&] (scrimmage::MessagePtr<std::vector<scrimmage::ID>> msg) {
            jammed_ids_.clear(); //clear data from previous msg
            for (size_t i = 0; i < msg->data.size(); i++) {
                jammed_ids_.push_back(msg->data[i]); //vector containing the ids of all jammed agents
            }
			if (std::find(jammed_ids_.begin(), jammed_ids_.end(), parent_->id()) != jammed_ids_.end()) {
				jammed_ = 1;
			} else {
				jammed_ = 0;
			}
        };
        subscribe<std::vector<scrimmage::ID>>("GlobalNetwork", "Jamming", jamming_cb);
        auto building_cb = [&] (scrimmage::MessagePtr<std::tuple<int, double, double>> msg) {
            building_sizes_[std::get<0>(msg->data)] = std::make_pair(std::get<1>(msg->data), std::get<2>(msg->data));
        };
        subscribe<std::tuple<int, double, double>>("GlobalNetwork", "BuildingSize", building_cb);

        auto flag_cb = [&] (scrimmage::MessagePtr<std::tuple<int, int, Eigen::Vector3d>> msg) {
            int team_id = std::get<0>(msg->data);
            Eigen::Vector3d location = std::get<2>(msg->data);
            if(team_id == parent_->id().team_id()) {
                my_flag_location_ = location;
            } else {
                enemy_flag_location_ = location;
				enemy_flag_id_ = std::get<1>(msg->data);
            }
        };
        subscribe<std::tuple<int, int, Eigen::Vector3d>>("GlobalNetwork", "FlagLocation", flag_cb);
    }

    void CTFSensor::find_closest_enemy(){
        double min_dist = std::numeric_limits<double>::infinity();
        for (auto &kv : *(parent_->contacts())) {
            sc::Contact &cnt = kv.second;
            if (cnt.id().team_id() == parent_->id().team_id()) {
                continue; // Ignore same team
            }
            if (closest_ == nullptr || (parent_->state()->pos() - closest_->pos()).norm() < min_dist) {
                closest_ = cnt.state();
                min_dist = (parent_->state()->pos() - closest_->pos()).norm();
            }
        }
    }

    bool CTFSensor::is_in_range(Contact &agent) {
        double sensing_range = sensing_range_;
        if(agent.id().team_id() == building_team_) {
            // so that you can see buildings at their edges.
            auto szpair = building_sizes_[agent.id().id()];
            double building_radius = Eigen::Vector3d(szpair.first, szpair.second, 0).norm();
            sensing_range += building_radius;
        }
        std::vector<scrimmage::ID>::iterator jammed_info = std::find(jammed_ids_.begin(), jammed_ids_.end(), parent_->id());
        if (jammed_info != jammed_ids_.end()){
            sensing_range = sensing_range / 4;
        }
        return (agent.state()->pos() - parent_->state()->pos()).norm() <= sensing_range;
    }

    int CTFSensor::get_heading_bin_to(const Eigen::Vector3d &obj) {
		Eigen::Vector3d to_obj = obj - parent_->state()->pos();
		return this->get_heading_bin(to_obj);
    }

    int CTFSensor::get_heading_bin(const Eigen::Vector3d &vec) {
        double heading = atan2(vec[1], vec[0]);
		heading = heading < 0.0 ? (M_PI*2-abs(heading)) : heading;

		return std::min(int( (heading / (M_PI*2)) * HEADING_SECTORS ), HEADING_SECTORS - 1);
    }

    int CTFSensor::get_heading_opposite_side() {
        int mask = parent_->id().team_id() == 1 ? -1 : 1; // 2 teams, positive and negative
		Eigen::Vector3d otherside = Eigen::Vector3d(mask * x_extent_, 0, 0);
		return this->get_heading_bin(otherside);
    }

    bool CTFSensor::in_base(const int& boundary_id) {
        auto it = boundaries_.find(boundary_id);
        return it != boundaries_.end() && std::get<1>(it->second)->contains(parent_->state()->pos());
    }

    int CTFSensor::get_team_space() {
        // Friendly: 0
        // Neutral: 1
        // Enemy: 2
        if(in_base(team_space_boundary_id_)) {
            return 0;
        } else if(in_base(enemy_space_boundary_id_)) {
            return 2;
        } else {
            return 1;
        }
    }

    void CTFSensor::get_observation(int* data, uint32_t beg_idx, uint32_t end_idx) {
        if (parent_ == NULL){
            std::cout << "*******  AGENT REMOVED ****** " << std::endl;
            return;
        }

        uint32_t beginning = beg_idx;

        // update team_space_
        team_space_ = get_team_space();

        // Add current entity observations
        data[beginning++] = get_heading_opposite_side();
        data[beginning++] = jammed_;
        data[beginning++] = has_flag_;
        data[beginning++] = team_space_;

        // Add friendly observations
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() != parent_->id().team_id() || kv.second.id().id() == parent_->id().id()) {
                continue; // Ignore enemy team
            }
            if (is_in_range(kv.second) || kv.second.id().id() == flag_id_) {
                auto &friend_pos = kv.second.state()->pos();
                data[beginning++] = get_heading_bin_to(friend_pos);
            } else {
                data[beginning++] = HEADING_SECTORS;
            }
        }

        // Add enemy observations
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() == parent_->id().team_id() || kv.second.id().team_id() == building_team_) {
              continue; // Ignore same team
            }
            if (is_in_range(kv.second) || (kv.second.id().id() == enemy_flag_id_ && seen_ent_ids_.find(enemy_flag_id_) != seen_ent_ids_.end())) {
                if (kv.second.id().id() == enemy_flag_id_ && seen_ent_ids_.find(enemy_flag_id_) == seen_ent_ids_.end()) seen_ent_ids_.insert(enemy_flag_id_);
            }
            if (is_in_range(kv.second)) {
                auto &enemy_pos = kv.second.state()->pos();
                data[beginning++] = get_heading_bin_to(enemy_pos);
            } else {
                data[beginning++] = HEADING_SECTORS;
            }
        }

        // Add building observations
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() != building_team_) {
                continue; // Ignore non buildings
            }
            if (is_in_range(kv.second) || seen_ent_ids_.find(kv.second.id().id()) != seen_ent_ids_.end()) {
                if (seen_ent_ids_.find(kv.second.id().id()) == seen_ent_ids_.end()) seen_ent_ids_.insert(kv.second.id().id());
                auto &building_pos = kv.second.state()->pos();
                data[beginning++] = get_heading_bin_to(building_pos);
            } else {
                data[beginning++] = HEADING_SECTORS;
            }
        }

        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() == parent_->id().team_id()) {
                if (kv.second.id().id() != parent_->id().id() && kv.second.id().id() != flag_id_) {
                    if (check_los(kv.second)){
                        data[beginning++] = 1;
                    } else {
                        data[beginning++] = 0;
                    }
                }
            }
        }
        data[beginning++] = parent_->id().team_id();
    }

    bool CTFSensor::check_los(sc::Contact& contact) {
        Eigen::Vector3d entity2_pos = contact.state()->pos();
        Eigen::Vector3d delta = parent_->state()->pos() - entity2_pos;
        double distance = delta.norm();
        std::list<Eigen::Vector3d> pts;
        for(int i = 0; i < distance; i++) {
            Eigen::Vector3d newpt = entity2_pos + i*delta / (distance);
            pts.push_back(newpt);
        }
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() == building_team_) {
                double x = kv.second.state()->pos()(0);
                double y = kv.second.state()->pos()(1);
                double width = building_sizes_[kv.second.id().id()].first;
                double height = building_sizes_[kv.second.id().id()].second;
                double boundx0 = x + width/2;
                double boundx1 = x - width/2;
                double boundy0 = y + height/2;
                double boundy1 = y - height/2;
                for (auto ppoint = pts.begin(); ppoint != pts.end(); ++ppoint) {
                    auto pt = *ppoint;
                    if(pt[0] <= std::max(boundx0, boundx1) && pt[0] >= std::min(boundx0, boundx1)) {
                        if(pt[1] <= std::max(boundy0, boundy1) && pt[1] >= std::min(boundy0, boundy1)) {
                            // intersection with building!!! Return false
                            return false;
                        }
                    }
                }
            }
        }
        // no intersection detected
        return true;
    }

    void CTFSensor::get_observation(double *data, uint32_t beg_idx, uint32_t end_idx) {
        uint32_t beginning = beg_idx;
        data[beginning++] = parent_->state()->pos()(0);
        data[beginning++] = parent_->state()->pos()(1);

        // publish distance from flag
        std::tuple<int, int, double> dist = std::tuple<int, int, double>(parent_->id().team_id(), parent_->id().id(), (parent_->state()->pos() - enemy_flag_location_).norm());
        auto dist_msg = std::make_shared < sc::Message<std::tuple<int, int, double>> >(dist);
        dist_msg->data = dist;
        pub_flag_dist_->publish(dist_msg);

        // Add friendly observations
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() != parent_->id().team_id() || kv.second.id().id() == parent_->id().id()) {
              continue; // Ignore enemy team
            }

            if (is_in_range(kv.second) || kv.second.id().id() == flag_id_) {
                auto &friend_pos = kv.second.state()->pos();
                data[beginning++] = friend_pos(0);
                data[beginning++] = friend_pos(1);
                double distance = (friend_pos - parent_->state()->pos()).norm();
                data[beginning++] = distance;
            } else {
                // Replace all information for agents that are out of range
                data[beginning++] = 0;
                data[beginning++] = 0;
                data[beginning++] = sensing_range_;
            }
        }

        // Add enemy observations
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() == parent_->id().team_id() || kv.second.id().team_id() == building_team_) {
                continue; // Ignore same team
            }
            if (is_in_range(kv.second) || (kv.second.id().id() == enemy_flag_id_ && seen_ent_ids_.find(enemy_flag_id_) != seen_ent_ids_.end())) {
                if (kv.second.id().id() == enemy_flag_id_ && seen_ent_ids_.find(enemy_flag_id_) == seen_ent_ids_.end()) seen_ent_ids_.insert(enemy_flag_id_);
                auto &enemy_pos = kv.second.state()->pos();
                data[beginning++] = enemy_pos(0);
                data[beginning++] = enemy_pos(1);
                double distance = (enemy_pos - parent_->state()->pos()).norm();
                data[beginning++] = distance;
            } else {
                // Replace all information for agents that are out of range
                data[beginning++] = 0;
                data[beginning++] = 0;
                data[beginning++] = sensing_range_;
            }
        }

        // Add building observations
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() != building_team_) {
                continue; // Ignore non buildings
            }
            if (is_in_range(kv.second) || seen_ent_ids_.find(kv.second.id().id()) != seen_ent_ids_.end()) {
                if (seen_ent_ids_.find(kv.second.id().id()) == seen_ent_ids_.end()) seen_ent_ids_.insert(kv.second.id().id());
                auto &building_pos = kv.second.state()->pos();
                data[beginning++] = building_pos(0);
                data[beginning++] = building_pos(1);
                double distance = (building_pos - parent_->state()->pos()).norm();
                data[beginning++] = distance;
                auto &size = building_sizes_[kv.second.id().id()];
                data[beginning++] = size.first;
                data[beginning++] = size.second;
            } else {
              // Replace all information for buildings that are out of range
              data[beginning++] = 0;
              data[beginning++] = 0;
              data[beginning++] = sensing_range_;
              data[beginning++] = 0;
              data[beginning++] = 0;
            }
        }
    }

	void CTFSensor::set_observation_space() {
        const double inf = std::numeric_limits<double>::infinity();

        // Current entity state
        observation_space.continuous_extrema.emplace_back(-inf, inf); // entity x
        observation_space.continuous_extrema.emplace_back(-inf, inf); // entity y
        observation_space.discrete_count.push_back(HEADING_SECTORS); //heading to opposite end of map
        observation_space.discrete_count.push_back(2); // jam flag
        observation_space.discrete_count.push_back(2); // carrying flag
        observation_space.discrete_count.push_back(3); // team space flag

        // Friendly state
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() != parent_->id().team_id() || kv.second.id().id() == parent_->id().id()) {
                continue; // Ignore enemy team
            }

            observation_space.continuous_extrema.emplace_back(-inf, inf); // friend x
            observation_space.continuous_extrema.emplace_back(-inf, inf); // friend y
            observation_space.continuous_extrema.emplace_back(0, inf); // distance
            observation_space.discrete_count.push_back(HEADING_SECTORS + 1); //heading to friend
        }

        // Enemy state
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() == parent_->id().team_id() || kv.second.id().team_id() == building_team_) {
                continue; // Ignore same team
            }
            observation_space.continuous_extrema.emplace_back(-inf, inf); // enemy x
            observation_space.continuous_extrema.emplace_back(-inf, inf); // enemy y
            observation_space.continuous_extrema.emplace_back(0, inf); // distance
            observation_space.discrete_count.push_back(HEADING_SECTORS + 1); //heading to enemy
        }

        // Buildings
        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() != building_team_) {
                continue; // Ignore non buildings
            }
            observation_space.continuous_extrema.emplace_back(-inf, inf); // building center x
            observation_space.continuous_extrema.emplace_back(-inf, inf); // building center y
            observation_space.continuous_extrema.emplace_back(0, inf); // distance
            observation_space.discrete_count.push_back(HEADING_SECTORS + 1); //heading to building center
            observation_space.continuous_extrema.emplace_back(0, inf); // x length of building
            observation_space.continuous_extrema.emplace_back(0, inf); // y length of building
        }

        for (auto &kv : *(parent_->contacts())) {
            if (kv.second.id().team_id() == parent_->id().team_id()) {
                  if (kv.second.id().id() != parent_->id().id() && kv.second.id().id() != flag_id_) { // remove flag from entity list without knowing who flag is.
                      observation_space.discrete_count.push_back(2); //LOS flag for friendly agents
                  }
            }
        }
        observation_space.discrete_count.push_back(3); // team id
    }
} // namespace sensor
} // namespace scrimmage
